from admin_reorder.middleware import ModelAdminReorder


class ModelAdminReorder(ModelAdminReorder):

    def admin_url_list(self, models):
        arr = []
        for per in models:
            if "admin_url" in per:
                arr.append(per['admin_url'])
                pass
            pass
        return arr
        pass

    def get_app_list(self):

        ordered_app_list = []
        for app_config in self.config:
            app = self.make_app(app_config)
            if app:
                # print(app)
                # app_config.update(app)
                app["in_config"] = app_config
                app["admin_urls"] = self.admin_url_list(app['models'])
                ordered_app_list.append(app)

        return ordered_app_list

        pass

    pass
