"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from .site import admin
from django.urls import path, include
from django.conf.urls import url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings
from .views.address_views import get_municipalities, \
get_barangay
from .views.tiny_mce_views import tinymce_upload_images

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("auth2.urls")),
    path('tinymce/', include('tinymce.urls')),
    path('get_municipalities/', get_municipalities, name="get_municipalities"),
    path('get_barangay/', get_barangay, name="get_municipalities"),
    path('tinymce_upload_files', tinymce_upload_images, name="tinymce_upload_image"),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
