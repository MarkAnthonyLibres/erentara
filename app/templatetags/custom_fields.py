from django import template
register = template.Library()
from urllib.parse import urlparse
from django.forms.widgets import FileInput
import base64
from django.core.files.storage import default_storage

@register.filter
def print_dir(value):
   print(dir(value))
   return dir(value);

@register.filter
def is_file_input_field(value):
   return isinstance(value, FileInput)
   pass    

@register.filter
def has_image_preview(value):
   widget = value.field.field.widget
   widget_attrs = widget.attrs
   return "with_image_preview" in widget_attrs and isinstance(widget, FileInput)
   pass

@register.filter
def to_field_hidden(value):

   class_attr = value.field.widget.attrs['class'] \
      if "class" in value.field.widget.attrs else ""

   value.field.widget.attrs["class"] = str(str(class_attr) + " hidden").strip()

   return value

   pass

@register.filter
def add_custom_class(value, custom_class):
   
   class_attr = value.field.widget.attrs['class'] \
      if "class" in value.field.widget.attrs else ""

   value.field.widget.attrs["class"] = str(str(class_attr) + " " + str(custom_class)).strip()
   return value
   pass

@register.filter
def img_default(value, default_value):

   value = str(value) if value else None
   if not value:
      return default_value

   is_exist = default_storage.exists(value)
   
   if is_exist:
      return value
   
   return default_value

   pass