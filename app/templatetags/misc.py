from django.contrib.admin.templatetags.base import InclusionAdminNode
from django.contrib.admin.templatetags.admin_list import result_list as base_result_list, \
    result_headers as base_result_headers
from django.contrib.admin.utils import label_for_field
from django import template
import json

register = template.Library()

def compile_list_display(model_admin):

    cl_list_display = model_admin.list_display or []
    if len(model_admin.list_display_temp) > 0:
        cl_list_display = model_admin.list_display_temp or list()
        pass

    model_admin.list_display = []
    model_admin.list_display_temp = []

    for per in list(cl_list_display):

        if not str(per) in dict(model_admin.form.base_fields):
            continue
            pass

        if isinstance(per, str):
            model_admin.list_display.append(per)
            default_attr_for_per = {
                "field" : per,
                "type" : "text",
                "form_field" : model_admin.form.base_fields[per]
            }
            model_admin.list_display_temp.append(default_attr_for_per)
            continue
            pass
        if isinstance(per, dict):
            for per_attr in ["field", "type", "form_field"]:
                if not per_attr in per:
                    error_str = "list display field on {field_text} has no attribute for {attr} "\
                        .format(field_text="", attr=per_attr)
                    raise AttributeError(error_str)
                    pass
                pass
            model_admin.list_display.append(per['field'])
            model_admin.list_display_temp.append(per)
            continue
            pass
        raise AttributeError("invalid value for list display")
        pass
    return model_admin
    pass


def get_display_attrs(cl):

    cl_base = cl
    print(cl.model_admin.list_display)

    # print(cl.model_admin.list_display_temp)
    # print(cl.model_admin.list_display)
    # print(cl_base.model_admin.form.base_fields)
    # gaga = cl_base.model_admin.list_display_temp
    # print(gaga[3]['form_field'].queryset)

    headers = list(base_result_headers(cl_base))

    if hasattr(cl_base.model_admin, "list_display_attr"):
        label_attrs = {}

        for per in cl_base.model_admin.list_display_attr:
            text, attr = label_for_field(
                per, cl_base.model,
                model_admin=cl_base.model_admin,
                return_attr=True
            )
            label_attrs[text] = per

            pass

        for per in headers:
            text = per["text"]
            if text in label_attrs:
                per["attrs"] = cl_base.model_admin.list_display_attr[label_attrs[text]]
                fields = list(cl_base.model_admin.form.base_fields)
                # print(fields)
                pass
            pass

        pass

    return headers
    pass


def result_headers(cl):
    # headers = list(base_result_headers(cl))
    headers = get_display_attrs(cl)
    return headers
    pass


def result_list(cl):
    context = base_result_list(cl)
    context["model_admin"] = cl.model_admin
    context["result_headers"] = list(result_headers(cl))
    return context


@register.filter(name='to_json')
def to_json(value):
    import simplejson
    arr = list()
    for per in value:
        inner_arr = dict()
        for per_attr in per.keys():
            if isinstance(per[per_attr], str):
                inner_arr[per_attr] = per[per_attr]
                continue
                pass
            try :
                inner_arr[per_attr] = simplejson.dumps(per[per_attr])
                pass
            except Exception as e:
                inner_arr[per_attr] = str(per[per_attr])
                pass

            pass
        arr.append(inner_arr)
        pass
    return simplejson.dumps(arr)
    pass


@register.filter(name='attrs_compile')
def attrs_compile(value):
    val = []
    for per in value.keys():
        per_attr = ""
        if isinstance(value[per], dict):
            attr_values = ""
            for per_values in dict(value[per]).keys():
                attr_values += per_values + ":" + str(value[per][per_values]) + ";"
                pass
            per_attr += per + '=' + attr_values
        else:
            per_attr += per + '=' + str(value[per])
            pass
        val.append(per_attr)
        pass

    return " ".join(val)

    pass


@register.tag(name='result_list')
def result_list_tag(parser, token):
    return InclusionAdminNode(
        parser, token,
        func=result_list,
        template_name='change_list_results.html',
        takes_context=False
    )
