from django.contrib import admin
from django.contrib.admin.views.main import ChangeList
from django.template.response import TemplateResponse


class AdminChangeList(ChangeList):
    modeladmin = None
    change_list_view = None

    def __init__(self, request,
                 model,
                 list_display,
                 list_display_links,
                 list_filter,
                 date_hierarchy,
                 search_fields,
                 list_select_related,
                 list_per_page,
                 list_max_show_all,
                 list_editable,
                 model_admin,
                 sortable_by,
                 ):
        super().__init__(request,
                         model=model,
                         list_display=list_display,
                         list_display_links=list_display_links,
                         list_filter=list_filter,
                         date_hierarchy=date_hierarchy,
                         search_fields=search_fields,
                         list_select_related=list_select_related,
                         list_per_page=list_per_page,
                         list_max_show_all=list_max_show_all,
                         list_editable=list_editable,
                         model_admin=model_admin,
                         sortable_by=sortable_by
                         )

        self.model_admin = model_admin

    pass

    pass


class ModelAdmin(admin.ModelAdmin):
    template_modules = []
    extra_js = []
    extra_css = []
    fieldset_template = None
    add_readonly_fields = []
    change_list_table_title = None
    change_list_table_subtitle = None
    change_list_template = "admin/theme/theme.changelist.html"
    list_display_temp = []

    def __init__(self, model, admin_site):
        # from app.templatetags.misc import compile_list_display
        # compile_list_display(self)
        super(ModelAdmin, self).__init__(model, admin_site)
        pass

    def get_list_display(self, request):
        # print(self.list_display)
        # from app.templatetags.misc import compile_list_display
        # compile_list_display(self)
        # print("Gago>>")
        # print(self.list_display)
        # return self.list_display

        return ("banner_src",)

        pass

    def __context_misc__(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['template_modules'] = self.template_modules
        extra_context["extra_js"] = self.extra_js
        extra_context["extra_css"] = self.extra_css
        extra_context["fieldset_template"] = self.fieldset_template
        extra_context["fieldset_template"] = self.fieldset_template
        extra_context["app_list"] = admin.site.get_app_list(request)
        extra_context["admin"] = self
        return extra_context
        pass

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields
        else:
            return self.add_readonly_fields
        pass

    def add_view(self, request, form_url=None, extra_context=None):
        extra_context = self.__context_misc__(request, extra_context)
        return super().add_view(request, form_url=form_url, extra_context=extra_context)

    def change_view(self, request, object_id=None, form_url=None, extra_context=None):
        extra_context = self.__context_misc__(request, extra_context)
        if form_url:
            return super().change_view(request, object_id, form_url=form_url, extra_context=extra_context)
            pass
        return super().change_view(request, object_id, extra_context=extra_context)

    def changelist_view(self, request, extra_context=None):

        from app.templatetags.misc import result_headers

        extra_context = self.__context_misc__(request, extra_context)
        response = super().changelist_view(request, extra_context=extra_context)

        if self.admin_site.theme_base_path:
            cl = response.context_data['cl']
            response.context_data["list_display_headers"] = result_headers(cl)
            pass

        return response

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = self.__context_misc__(request, extra_context)

        return super().changeform_view(request, object_id, form_url, extra_context)
        pass

    def get_changelist(self, request, **kwargs):
        return AdminChangeList
        pass

    pass
