from django.shortcuts import redirect, HttpResponseRedirect, reverse
from django.contrib.auth.decorators import login_required
from ph_geography.models import Municipality, Province, Barangay
from django.http import JsonResponse
from django.core import serializers
from django.db.models import CharField, Value
from django.db.models.functions import Concat
from django.db.models import Q
import json





@login_required
def get_municipalities(request):
    province_id = request.GET.get("province")
    model_province = Province.objects.get(id=province_id)

    all_municipalities = Municipality \
        .objects \
        .filter(province=model_province) \
        .order_by("name") \
        .values("id", "name", "code")

    as_json = json.dumps(list(all_municipalities))

    return JsonResponse(as_json, safe=False)

    pass


@login_required
def get_barangay(request):
    municipality_id = request.GET.get("municipality")
    model_municiaplity = Municipality.objects.get(id=municipality_id)

    all_municipalities = Barangay \
        .objects \
        .filter(municipality=model_municiaplity) \
        .order_by("name") \
        .values("id", "name", "code")

    as_json = json.dumps(list(all_municipalities))

    return JsonResponse(as_json, safe=False)
    pass


