from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from product.models import MCEFile

@login_required
@csrf_exempt
def tinymce_upload_images(request):
    that_file = MCEFile.objects.create(
        user=request.user,
        file=request.FILES['file']
    )
    return JsonResponse({"location" : that_file.file.url})
    pass