from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from auth2.models import UserInformation
from django.db.models import Q
from django.contrib import admin

def get_filename(filename):
    return filename.upper()


class EmailBackend(ModelBackend):
    def authenticate(self, request, email=None, password=None,username = None, **kwargs):

        try:
            if email:
                user = UserInformation.objects.get(email=email)
            else:
                user = UserInformation.objects.get(username=username)
            pass       
        except UserInformation.DoesNotExist:
            return None
        else:
            if user.check_password(password):
                return user
        return None


def custom_titled_filter(title):
    class Wrapper(admin.FieldListFilter):
        def __new__(cls, *args, **kwargs):
            instance = admin.FieldListFilter.create(*args, **kwargs)
            instance.title = title
            return instance
    return Wrapper