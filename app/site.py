from django.contrib.admin.sites import AdminSite
from django.contrib.auth.models import Group, User
from auth2.models import UserInformation

from product.admin import CategoryAdmin, ProductAdmin
from product.models import  Category, Products
from auth2.admin import UserAdmin, AuthGroupAdmin

from django.contrib import admin



class MyAdminSite(AdminSite):
    index_title = "Welcome back, User!"
    theme_base_path = None

    def index(self, request, extra_context=None):
        if extra_context is None:
            extra_context = {}
            
        if not request.user.is_anonymous:

            self.index_title = "Welcome back, {firstname}!" \
            .format(firstname=request.user.first_name)

            pass    
        # print(extra_context)
        return super().index(request, extra_context)

    def each_context(self, request):
        default_context = super().each_context(request)
        default_context["admin_site"] = self
        return default_context
        pass

    def _build_app_dict(self, request, label = None):
        app_dict = super()._build_app_dict(request, label=label) 
        # print(app_dict) 
        return app_dict
        exit(0)

    def get_app_list(self, request):
        app_list = super().get_app_list(request)
        return app_list
        pass

admin.site = MyAdminSite()
admin.site.site_header = "Erentara"
admin.site.site_title = "Erentara"
admin.site.site_url = "/home"
admin.site.theme_base_path = "admin/theme/theme.base.html"
admin.site.login_template = "admin/theme/theme.login.html"

admin.autodiscover()

admin.site.register(Group, AuthGroupAdmin)
# admin.site.register(User, UserAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(UserInformation, UserAdmin)
admin.site.register(Products, ProductAdmin)
