# Generated by Django 3.2.5 on 2021-07-24 10:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ph_geography', '0002_auto_20210724_0734'),
    ]

    operations = [
        migrations.AlterField(
            model_name='barangay',
            name='is_urban',
            field=models.BooleanField(default=False, verbose_name='Is Urban'),
        ),
    ]
