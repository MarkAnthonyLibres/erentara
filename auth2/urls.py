from django.urls import path
from .views import signup, forgotpassword, LoginFormView, home, logout_view

urlpatterns = [
    path('login/', LoginFormView.as_view(), name="signin"),
    path('logout/', logout_view, name="logout"),
    path('signup/', signup, name="signup"),
    path('forgotpassword/', forgotpassword, name="forgot"),
    path('home/', home, name="home"),
    # path('login/', ),
]