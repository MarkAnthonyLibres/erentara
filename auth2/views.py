from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth import authenticate, login
from django.views.generic import TemplateView
from .forms.auth_form import LoginForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator
from django.contrib.auth import logout


# Create your views here.

def should_not_login(user):
    print(user)
    return user.is_anonymous
    pass

def login_view(request):
    return render(request, template_name="auth/signin.html")
    pass


def signup(request):
    return render(request, template_name="auth/signup.html")
    pass

def home(request):
    return render(request, template_name="home/home.html")
    pass

def forgotpassword(request):
    return render(request, template_name="auth/forgot.html")
    pass


def logout_view(request):
    logout(request)
    return redirect('signin')
    pass

class LoginFormView(TemplateView):


    def get(self, request):
        # logout(request)
        if request.user.is_authenticated:
            return redirect("home")
            pass
        
        return render(request, template_name="auth/signin.html", context={
            "form": LoginForm
        })
        pass


    
    def post(self, request):

        form = LoginForm(request.POST)

        if not form.is_valid():
            raise Exception("Form is not valid.")

        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        user = authenticate(email=email, password=password)

        if not user:
            raise Exception("Please sign up instead")

        login(request, user)
        return redirect('home')

        pass

