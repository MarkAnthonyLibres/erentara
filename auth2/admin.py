from django.contrib.auth import models
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _
from django.contrib import admin
from app.admin_base import ModelAdmin
from .forms.users_form import OfUserChangeForm, AddUserForm
from django.contrib.auth.admin import GroupAdmin

class AuthGroupAdmin(GroupAdmin, ModelAdmin):
    change_list_table_title = "Group Lists"
    def __init__(self, model, admin_site = None):
        super().__init__(model, admin_site)
    pass


class UserAdmin(ModelAdmin, UserAdmin):
    form = OfUserChangeForm
    add_form = AddUserForm
    extra_js = ("custom/js/address.js","custom/js/add_user.js")

    list_filter = ["is_superuser", 'is_stackholder', 'is_courrier']
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': (
            'first_name', 
            'last_name', 
            'middle_name', 
            'email',
            'gender', 
            'contact_number',
            'province',
            'municipalities',
            'barangay',
            'full_address'
        )}),
        (_('Type'), {
            'fields': ('is_superuser', 'is_stackholder', 'is_courrier'),
        }),
        (_('Permissions'), {
            'fields': ('groups', 'user_permissions',),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    # add_readonly_fields = ["is_staff"]

    add_fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': (
            'first_name', 
            'last_name', 
            'middle_name', 
            'email',
            'gender', 
            'contact_number',
            'province',
            'municipalities',
            'barangay',
            'full_address'
        )}),
        (_('Type'), {
            'fields': ( 'is_superuser', 'is_stackholder', 'is_courrier'),
        }),
    )

    def get_readonly_fields(self, request, obj=None):
        if not request.user.is_superuser:
            return ('is_active',
                    'is_staff',
                    'is_superuser',
                    'groups',
                    'user_permissions',
                    'last_login',
                    'date_joined'
                    )
            pass

        if obj and obj.is_superuser:
            return (
                'groups',
                'user_permissions',
                'last_login',
                'date_joined'
            )
            pass

        return super().get_readonly_fields(request, obj)

    def has_view_permission(self, request, obj=None):
        return request.user.is_superuser
        pass

    def has_change_permission(self, request, obj=None):
        if request.user == obj: return True
        if not request.user.is_superuser: return False
        return super().has_change_permission(request, obj)
        pass

    def has_delete_permission(self, request, obj=None):
        if not request.user.is_superuser: return False
        return super().has_delete_permission(request, obj)
        pass

    def get_queryset(self, request):
        query = super().get_queryset(request)
        if not request.user.is_superuser:
            return query.filter(id=request.user.id)
            pass
        return query
        pass


    pass


