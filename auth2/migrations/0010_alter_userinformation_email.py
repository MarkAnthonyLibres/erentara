# Generated by Django 3.2.6 on 2021-08-08 18:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth2', '0009_products_initial_preview'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userinformation',
            name='email',
            field=models.EmailField(max_length=254, unique=True),
        ),
    ]
