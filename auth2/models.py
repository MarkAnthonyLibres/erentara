from django.contrib.auth.models import AbstractUser
from django.db import models
from ph_geography.models import Province, Municipality, Barangay

class UserInformation(AbstractUser, models.Model):

    class GenderChoices(models.IntegerChoices):
        MALE = 1, 'Male'
        FEMALE = 2, 'Female'

    class UserType(models.IntegerChoices):
        ADMIN = 1, "Admin"
        STACKHOLDER = 2, "Stackholder",
        COURRIER = 3, "Courrier"    

    middle_name = models.CharField(blank=False, null=True, max_length=255)
    contact_number = models.CharField(blank=False, null=False, max_length=10)
    is_stackholder = models.BooleanField(blank=False, null=False, default=False)
    is_courrier = models.BooleanField(blank=False, null=False, default=False)
    email = models.EmailField(blank=False, null=False, unique=True)
    gender = models.IntegerField(choices=GenderChoices.choices, blank=False, null=True, default=None)
    province = models.ForeignKey(Province, on_delete=models.DO_NOTHING, blank=False, null=True, default=None)
    municipalities = models.ForeignKey(Municipality, on_delete=models.DO_NOTHING, blank=False, null=True, default=None)
    barangay = models.ForeignKey(Barangay, on_delete=models.DO_NOTHING, blank=False, null=True, default=None)
    full_address = models.CharField(blank=False, null=True, max_length=250, default=None)

    pass

