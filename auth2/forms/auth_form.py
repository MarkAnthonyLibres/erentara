from django import forms
from ..models import UserInformation


class LoginForm(forms.Form):
    email = forms.CharField(required=True, widget=forms.EmailInput(
        attrs={
            "class":"form-control form-control-solid h-auto py-7 px-6 rounded-lg border-0",
            "autocomplete":"off"
        }
    ))
    password = forms.CharField(required=True, widget=forms.PasswordInput(
        attrs={
            "class":"form-control form-control-solid h-auto py-7 px-6 rounded-lg border-0",
            "autocomplete":"off"
        }
    ))