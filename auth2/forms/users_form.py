from django import forms
from ph_geography.models import Province, Municipality, Barangay
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from ..models import UserInformation
from django.utils.translation import gettext, gettext_lazy as _
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UsernameField
from app.admin_base import ModelAdmin

"is_staff", "is_superuser"
class UserFields(forms.ModelForm):

    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    middle_name = forms.CharField(required=False)
    gender = forms.ChoiceField(required=True,choices=UserInformation.GenderChoices.choices, widget=forms.RadioSelect())
    is_staff = forms.BooleanField(
        required=False,
        label="Active",
        help_text="Designates whether the user can log into this admin site.",
        initial=True,
        widget=forms.CheckboxInput(attrs={"input_role" : "user_type"})
    )
    is_superuser = forms.BooleanField(
        required=False,
        label="Administrator",
        help_text="Designates that this user has all permissions without explicitly assigning them.",
         widget=forms.CheckboxInput(attrs={"input_role" : "user_type"})
    )
    is_stackholder = forms.BooleanField(
        required=False,
        label="Stack holder",
        help_text="Designates that this user can add or update her rent list.",
         widget=forms.CheckboxInput(attrs={"input_role" : "user_type"})
    )
    is_courrier = forms.BooleanField(
        required=False,
        label="Courrier",
        help_text="Designates that this user can approved rental request from other users",
         widget=forms.CheckboxInput(attrs={"input_role" : "user_type"})
    )
    province = forms.ModelChoiceField(
        queryset=Province.objects.all(),
        required=True,
        widget=forms.Select(attrs={'data_type': 'input_province', 'enable-loading' : True})
    )
    municipalities = forms.ModelChoiceField(
        queryset=Municipality.objects.none(),
        required=True,
        widget=forms.Select(attrs={'data_type': 'input_province', 'enable-loading' : True})
    )
    barangay = forms.ModelChoiceField(
        queryset=Barangay.objects.none(),
        required=False,
        widget=forms.Select(attrs={'data_type': 'input_barangay', 'enable-loading' : True})
    )
    full_address = forms.CharField(
        required=True,
        label="Full Address",
        widget=forms.Textarea(attrs={'data_type': 'input_address'})
    )
    contact_number = forms.CharField(
        required=True,
        label="Phone number (+63)",
        widget=forms.TextInput(attrs={"autocomplete": "off"})
    )

    def clean_contact_number(self):
        contact_number = self.cleaned_data.get('contact_number')
        if ' ' in contact_number:
            raise forms.ValidationError('Should not have blank spaces included')
        if not str(contact_number).isdigit():
            raise forms.ValidationError('Should not have letters or special characters included')
            pass
        return contact_number

        pass

    def on_initial(self):

        if "province" in self.initial and self.initial['province']:
            self.fields['municipalities'].queryset = Municipality \
                .objects \
                .filter(province=self.initial['province'])

            self.fields['province'].widget.attrs["input_value"] = self.initial['province'];


        if "municipalities" in self.initial and self.initial['municipalities']:
            self.fields['barangay'].queryset = Barangay \
                .objects \
                .filter(municipality=self.initial['municipalities'])
            pass


        pass

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.fields['municipalities'].queryset = Municipality.objects.none()

        if "province" in self.data and self.data['province']:
            self.fields['municipalities'].queryset = Municipality\
                .objects\
                .filter(province=self.data['province'])

            self.fields['province'].widget.attrs["input_value"] = self.data['province'];

        if "municipalities" in self.data and self.data['municipalities']:

            self.fields['barangay'].queryset = Barangay \
                .objects \
                .filter(municipality=self.data['municipalities'])

            self.fields['barangay'].widget.attrs["input_value"] = self.data['barangay'];

            pass

        self.on_initial()

    pass

class OfUserChangeForm(UserFields, UserChangeForm):
    pass


class AddUserForm(UserFields):
    password = forms.CharField(
        label=_("Password",),
        strip=False,
        widget=forms.TextInput(attrs={'autocomplete': 'new-password'}),
        help_text=password_validation.password_validators_help_text_html())

    class Meta:
        model = UserInformation
        fields = ("username",)
        field_classes = {'username': UsernameField}

    pass