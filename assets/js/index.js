(function(fn) {

    fn(".image_container.with_image_preview").on("click", function() {
        const input = jQuery(this).parent("section").find("input[with_image_preview]");
        input.click();
    });

    $("button[data-dismiss=modal]").click(function() {
        $(".modal").modal('hide');
    });


    fn("input[with_image_preview]").change(function() {
        jQuery("#image_preview_modal").modal("show");
    });

})(jQuery)