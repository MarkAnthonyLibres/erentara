(function(fn) {

 const tables = fn("[jsgrid_table]").get();
 tables.forEach(function(self) {
    const $self = fn(self);
    $self.jsGrid({
        height: "90%",
        width: "100%",
        filtering: true,
        editing: true,
        sorting: true,
        paging: true,
        autoload: true,

        pageSize: 15,
        pageButtonCount: 5,

        deleteConfirm: "Do you really want to delete the client?",
        get fields() {

            const headers = window.main_table_headers;



        },

        __fields: [
            { name: "Name", type: "text", width: 150 },
            { name: "Age", type: "number", width: 50 },
            { name: "Address", type: "text", width: 200 },
            { name: "Country", type: "select", items: [], valueField: "Id", textField: "Name" },
            { name: "Married", type: "checkbox", title: "Is Married", sorting: false },
            { type: "control" }
        ]

    });
 });


})(jQuery)