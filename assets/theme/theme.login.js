window.Parsley.on('field:error', function () {

    Swal.fire({
        text: "Sorry, something went wrong, please try again.",
        icon: "error",
        buttonsStyling: false,
        confirmButtonText: "Ok, got it!",
        customClass: {
            confirmButton: "btn font-weight-bold btn-light-primary"
        }
    }).then(function() {
        KTUtil.scrollTop();
    });

});