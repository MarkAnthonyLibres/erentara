
/**
 * Customize Parsely Plugin
 */

(function(fn) {

    const parsleyConfig = {
        errorsContainer: function(parsleyField) {
            const fieldSet = parsleyField.$element.siblings(".fv-plugins-message-container");
            return fieldSet;
        }
    };

    fn("[data-parsley-validate]").parsley(parsleyConfig);

})(jQuery)