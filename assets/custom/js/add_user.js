(function(fn) {

    fn("input[input_role]").change(function() {
        const value = fn(this).attr("input_role");

        fn(`input[input_role="${value}"]`)
            .not(fn(this))
            .prop('checked', false);
            
    });

})(jQuery)