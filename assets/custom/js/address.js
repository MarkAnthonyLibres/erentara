window.addEventListener("load", function() {

(function(fn) {

    fn.fn.address = function(url, $select_element, params = new Function()) {

        return fn(this).change(function() {

            const $self = fn(this);
            if(!$self.val()) return;

            const enable_fields = function() {
                fn("[enable-loading]").removeAttr("disabled", true);
                $self.parent(".input-group")
                    .find(".enable-loading-container")
                    .css("display", "none")
            }

            const disable_fields = function() {
                fn("[enable-loading]").attr("disabled", true);
                $self.parent(".input-group")
                $self.parent(".input-group").find(".enable-loading-container")
                    .find(".enable-loading-container")
                    .css("display", "inline-block")
            }

            const request = fn.ajax({
                url : url,
                dataType : "json",
                data : params($self),
                beforeSend : disable_fields
            });

            request.then(function(result) {
                enable_fields();
                $select_element.html('');

                if((typeof result) == 'string')
                    result = JSON.parse(result);

                console.log($self.attr("input_value"))

                const opt_default = fn("<option>");
                opt_default.attr("value","")
                opt_default.html("---------");
                opt_default.attr("selected","");
                $select_element.append(opt_default);

                result.forEach(function(per_result){
                    const option = fn("<option>")
                    option.attr("value", per_result.id)
                    option.text(per_result.name);
                    $select_element.append(option)
                });

            });

            request.fail(function(err) {
                enable_fields();
                console.error(err);
                alert('Error occurred');
            });
        });

    }

    const municipalities_input = fn("select#id_municipalities");
    const province_input = fn("select#id_province");
    const barangay_input = fn("select#id_barangay")

    province_input.address("/get_municipalities",municipalities_input, function($self) {
        return { "province" : $self.val() }
    });

    municipalities_input.address("/get_barangay",barangay_input, function($self) {
        return { "municipality" : $self.val() }
    });



})(django.jQuery);

});

