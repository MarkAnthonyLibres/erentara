from django.db import models
from auth2.models import UserInformation
from tinymce import models as tinymce_models


# Create your models here.

class Category(models.Model):
    id = models.AutoField(primary_key=True)
    parent_category = models.ForeignKey('self', on_delete=models.CASCADE, blank=False, null=True)
    name = models.CharField(blank=False,null=False, max_length=255)

    def __str__(self) -> str:
        return self.name


    pass


class Products(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING, blank=False, null=False)
    title = models.CharField(blank=False, null=False, max_length=250)
    description = tinymce_models.HTMLField(blank=False, null=False)
    banner = models.ImageField(
        blank=False, 
        null=False, 
        default="default_imgs/initial_preview_product.jpg",
        upload_to='banners'
    )
    user = models.ForeignKey(
        UserInformation, 
        on_delete=models.CASCADE, 
        blank=False, 
        null=False, 
        default=None,
        verbose_name='Created by'
    )
    def __str__(self):
        return self.title
        pass

    pass

class MCEFile(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(UserInformation, on_delete=models.DO_NOTHING, blank=False, null=False)
    file = models.FileField(blank=False, null=True, upload_to='products_files/', default=None)
    pass


class Variants(models.Model):
    id = models.AutoField(primary_key=True)
    product = models.ForeignKey(Products, on_delete=models.DO_NOTHING, blank=False, null=False)
    name = models.CharField(max_length=200, blank=False, null=False)
    description = models.TextField(blank=False, null=False)
    pass