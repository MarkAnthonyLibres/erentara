from django.contrib import admin
from django import forms
from auth2.models import UserInformation
from .models import Products, Category, Variants
from app.admin_base import ModelAdmin
from tinymce.widgets import TinyMCE
from mce_filebrowser.admin import MCEFilebrowserAdmin
from app.utils import custom_titled_filter
from django.template import Engine
from django.template import Template, Context
from django.urls import reverse
from django.utils.safestring import mark_safe
from app.templatetags.custom_fields import img_default
from django.conf import settings

# Register your models here.


class CategoryForm(forms.ModelForm):
    parent_category = forms.ModelChoiceField(
        queryset=Category.objects.filter(parent_category=None),
        required=False,
        widget=forms.Select(attrs={"class": 'category'})
    )

    class Meta:
        model = Category
        fields = '__all__'

    pass


class CategoryAdmin(ModelAdmin):
    form = CategoryForm

    # add_form_template="admin/helpers_and_categories/category.html"

    def has_view_permission(self, request, obj=None):
        return request.user.is_superuser
        pass

    pass


class ProductAdminForm(forms.ModelForm):
    title = forms.CharField(required=True)
    description = forms.CharField(widget=TinyMCE(
        attrs={
            'cols': 80, 
            'rows': 30,
            "class" : "form-control"
        },
        mce_attrs = {
            "theme": "silver",
            "height": 500,
            "menubar": False,
            "images_upload_url": '/tinymce_upload_files', 
            "toolbar": "link image | undo redo | formatselect | "
            "bold italic | alignleft aligncenter "
            "alignright alignjustify | bullist numlist| "
            "removeformat | help",
            "images_upload_handler" : "tinymce_image_handler",
        }
    ))
    banner = forms.ImageField(
        label="Banner",
        required=True, 
        widget=forms.FileInput(attrs={'with_image_preview' : True}),
    )
    user = forms.ModelChoiceField(
        queryset=UserInformation.objects.all(),
        label="Created by"
    )
    # test = forms.ChoiceField(required=True, label="Variants")

    def save(self, commit: bool,  *args, **kwargs):
        self.instance.user = self.request.user
        return super().save(commit=commit)
        pass

    class Meta:
        fields = '__all__'
        model = Products


class VariantsInline(admin.TabularInline):
    model = Variants


class ProductAdmin(ModelAdmin):
    inlines = [VariantsInline,]
    template_modules = ["image_preview_modal"]
    form = ProductAdminForm
    readonly_fields = ('user',)
    list_display_temp = (
        {
            "field" : "banner_src",
            "type" : "text",
            "form_field" : ProductAdminForm.declared_fields["banner"]
        },
        {
            "field": "title",
            "type": "text",
            "form_field": ProductAdminForm.declared_fields["title"]
        },
        {
            "field": "category",
            "type": "select",
            "form_field": None
        },
        {
            "field": "per_user_display",
            "type": "text",
            "form_field": ProductAdminForm.declared_fields["user"]
        },

    )
    # list_display = ('banner_src', 'title', 'category', 'per_user_display')
    list_display_attr = {
        'banner_src' : {
            "th" : {
                "style" : {
                    "width" : "20%",
                },
            },
        },
        'title' : {
            "th" : {
                "style" : {
                    "width" : "20%",
                },
            },
        },
        'category' : {
            "th" : {
                "style" : {
                    "width" : "10%",
                },
            },
        }
    }
    list_filter = (
         ('category__name', custom_titled_filter('Category')),
    )
    fieldset_template = "admin/includes/fieldsets/products_fieldset.html"

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('category','title','banner','description')
        }),
    )

    fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'category',
                'title',
                'banner',
                'description'
            )
        }),
    )

    @admin.display(description='Posted by')
    def per_user_display(self, obj):
        url = reverse('admin:%s_%s_change' % (obj.user._meta.app_label,  obj.user._meta.model_name),  args=[obj.user.id] )
        link_template = u'<a href="{url}">{user}</a>'.format(user = obj.user.get_full_name(), url = url)
        return mark_safe(link_template)
        pass

    @admin.display(description='Banner')
    def banner_src(self, obj):
        image_value_link = obj.banner
        image_link = img_default(image_value_link, Products.banner.field.default)
        image_template = u'<image src="{media_url}{image}"width="120" height="100"/>'\
            .format(image = image_link, media_url = settings.MEDIA_URL)

        return mark_safe(image_template)
        pass

    def get_fieldsets(self, request, obj=None):
        if not obj:
            return self.add_fieldsets
        return super().get_fieldsets(request, obj)

    def get_queryset(self, request):
        query = super().get_queryset(request)
        if not request.user.is_superuser:
            return query.filter(user=request.user.id)
            pass
        return query
        pass    

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=obj, **kwargs)
        form.request = request
        return form
    


    def __def_permission__(self, request):

        if request.user.is_anonymous:
            return False

        if not (request.user.is_superuser or request.user.is_stackholder):
            return False 

        return True    

        pass

    def has_delete_permission(self, request, obj = None) -> bool:
        if not self.__def_permission__(request):
            return False

        return True 

    def has_change_permission(self, request, obj = None) -> bool:
        if not self.__def_permission__(request):
            return False

        return True 
        return super().has_change_permission(request, obj=obj)    

    def has_add_permission(self, request) -> bool:
       
        if not self.__def_permission__(request):
            return False

        return True 
        return super().has_add_permission(request)

    def has_module_permission(self, request) -> bool:

        if not self.__def_permission__(request):
            return False

        return True    
        return super().has_module_permission(request)
        
        pass

    def has_view_permission(self, request, obj=None):
  
        if not self.__def_permission__(request):
            return False

        return True     
        return super().has_view_permission(request) 

        pass

    pass
